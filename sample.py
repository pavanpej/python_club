import random
def game(user, comp):
	if comp == user:
		return 'Tie'
	elif comp == 'stone' :
		if user == 'paper':
			return 'User won'
		else:
			return 'User lost'

	elif comp == 'paper' :
		if user == 'stone':
			return 'User lost'
		else:
			return 'User won'

	else :
		if user == 'stone':
			return 'User Won'
		else:
			return 'User lost'

def main():
	data = ['stone','paper','scissors']
	comp = random.choice(data)
	user = input("Enter a choice: ")
	ans = game(user, comp)
	print("Computer: {0}".format(comp))
	print(ans)

if __name__ == '__main__':
	main()